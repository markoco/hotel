import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import './bootswatch.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import 'react-toastify/dist/ReactToastify.css';
import {toast} from 'react-toastify';

toast.configure({
  autoClose:3000,
  draggable: false,
  hideProgressBar: true,
  newestOnTop: true,
  closeOnClick: true,
  zIndex: 10
})

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

serviceWorker.unregister();
