import React, {useEffect,useState}  from 'react';
import ContactModal from '../../pages/ContactModal';
import { useHistory } from 'react-router-dom';

const defaultItems = [
    {
        name: 'Home',
        link: '/'
    },
    {
        name: 'Contact',
        link: ''
    },
    {
        name: 'Login',
        link: '/login'
    },
    {
        name: 'Register',
        link: '/register'
    }
]

const NavbarLogin = ({ mainContent }) => {

    const [navItems, setNavItems] = useState([]);
    const [showForm,setShowForm] = useState(false);
    const history = useHistory();

    useEffect(()=>{  
        setNavItems(defaultItems);
    },[])

    return (
        <>
            <nav 
                className="navbar navbar-expand-lg navbar-dark" 
                style={{
                        color:"white",
                        position:"absolute", 
                        top:0,
                        width:"100%",
                        zIndex:3
                    }}
            >
                <h1 
                    className="logo text-light" 
                >
                    Calaguas
                </h1>
                <button 
                    className="navbar-toggler" 
                    type="button" 
                    data-toggle="collapse" 
                    data-target="#navbarColor03" 
                    aria-controls="navbarColor03" 
                    aria-expanded="false" 
                    aria-label="Toggle navigation"
                >
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div 
                    className="collapse navbar-collapse"
                    id="navbarColor03"
                >
                    <ul className="navbar-nav ml-auto">
                        {navItems.map((navItem,index)=>(
                            navItem.link
                            ?
                            <li 
                                key={index} 
                                className="nav-item p-1 m-1" 
                                onClick={() => history.push(navItem.link)}
                            >
                                <a className="nav-link text-white">{navItem.name}</a>
                            </li>
                            :
                            <li 
                                className="nav-item p-1 m-1 text-danger" 
                                key={index} 
                                onClick={()=>setShowForm(true)}
                            >
                                <a href="#" className="nav-link text-white">{navItem.name}</a>
                            </li>
                        ))}
                    </ul>
                </div>
            </nav>
            <ContactModal showForm={showForm} toggleForm={()=>setShowForm(false)}/>
        </>
    )
}

export default NavbarLogin;