import React from 'react';

const Footer = () => {
    return (  
        <>
            <div 
                className="p-3 text-center text-light vw-100" 
                style={{backgroundColor:"black"}}
            >
                Copyright © 2019 Calaguas Hotel | All Rights Reserve
            </div>   
        </>
    );
}
 
export default Footer;