import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Input, Button, FormText, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import swal from 'sweetalert';
const RoomForm = ({showForm,toggleForm,saveRoom,updateRoom,isEditing,roomToEdit}) => {
    const[roomType,setRoomType] = useState('');
    const[description,setDescription] = useState('');
    const[price,setPrice] = useState('');
    const[roomCount,setRoomCount] = useState('');
    const[capacity,setCapacity] = useState('');
    const[area,setArea] = useState('');
    const[bathrooms,setBathrooms] = useState('');
    const[beds,setBeds] = useState('');
    const[imgPath,setImgPath] = useState('');
    const[aircon,setAircon] = useState(false);
    const[shower,setShower] = useState(false);
    const[television,setTelevision]  = useState(false);
    const[wifi,setWifi] = useState(false);

    const handleSaveRoom = async () => {
        if(roomType==='' || description==='' || price < 0 || roomCount < 0 || capacity < 0 || area < 0 || bathrooms < 0 || beds < 0  || !imgPath){
            swal("Ooops!", "Please complete all details!", "error");
        }else{
            let mainImage = await uploadMainImage(imgPath);
            saveRoom(
                roomType, 
                description, 
                price,
                roomCount,
                capacity,
                area,
                bathrooms,
                beds, 
                mainImage.imageUrl,
                aircon,
                shower,
                television,
                wifi 
                );
           setRoomType('');
           setDescription('');
           setPrice('');
           setRoomCount('');
           setCapacity('');
           setArea('');
           setBathrooms('');
           setBeds('');
           setImgPath('');
           setAircon(false);
           setShower(false);
           setTelevision(false);
           setWifi(false);
        }
    };

    const handleUpdateRoom = async () => {
        let mainImage = "";
        if(imgPath){
           mainImage = await uploadMainImage(imgPath);
           mainImage = mainImage.imageUrl;
        }else{
            mainImage = roomToEdit.imgPath
        }
        
        updateRoom(
             roomToEdit._id,
             roomType?roomType:roomToEdit.name, 
             description?description:roomToEdit.description, 
             price?price:roomToEdit.price,
             roomCount?roomToEdit:roomToEdit.count,
             capacity?capacity:roomToEdit.capacity,
             area?area:roomToEdit.area,
             bathrooms?bathrooms:roomToEdit.bathroom,
             beds?beds:roomToEdit.bed, 
             mainImage, 
             aircon,
             shower,
             television,
             wifi );
        setRoomType('');
        setDescription('');
        setPrice('');
        setRoomCount('');
        setCapacity('');
        setArea('');
        setBathrooms('');
        setBeds('');
        setImgPath('');
        setAircon(false);
        setShower(false);
        setTelevision(false);
        setWifi(false);
    };
    
    const selectMainImage = e =>{

        let image = e.target.files[0];
     
        if(image.name.match(/\.(jpg|JPG|JPEG|jpeg|gif|png)$/)){
            setImgPath(image);
        }else{
            console.log("error");
        }
        
    }

    const uploadMainImage = async (imageToSave) =>{
        const data = new FormData();
        data.append('image', imgPath, imageToSave.name);

        const imageData = await fetch('https://damp-woodland-92817.herokuapp.com/upload', {
            method: "POST",
            body: data
        })
        
        const imageUrl = await imageData.json();
      
        return imageUrl;
      
    }


    return (  
        <Modal className="rounded-0 modal-lg" isOpen={showForm} toggle={toggleForm}>
            <ModalHeader className="bg-primary" toggle={toggleForm}>
                <h1 className="text-light">
                    {isEditing?"Edit Room Details":"Add New Room Type"}
                </h1>
            </ModalHeader>
            <ModalBody>
              
                <FormGroup>
                    <Label>Room Type</Label>
                    <Input 
                        placeholder='Enter Room Type'
                        onChange={(e)=>setRoomType(e.target.value)}
                        defaultValue={roomToEdit.name}
                    />
                </FormGroup>
                <FormGroup>
                    <Label>Description</Label>
                    <Input 
                        type="textarea" 
                        placeholder='Enter Description'
                        onChange={(e)=>setDescription(e.target.value)}
                        defaultValue={roomToEdit.description}
                    />
                </FormGroup>
                <div className="d-flex">
                    <FormGroup className="w-50 mr-1">
                        <Label>Price</Label>
                        <Input 
                            placeholder='Enter Price'
                            type='number'
                            onChange={(e)=>setPrice(e.target.value)}
                            defaultValue={roomToEdit.price}
                        />
                    </FormGroup>
                    <FormGroup className="w-50 ml-1">
                        <Label>No. of Rooms</Label>
                        <Input 
                            placeholder='No. of Rooms'
                            type='number'
                            onChange={(e)=>setRoomCount(e.target.value)}
                            defaultValue={roomToEdit.count}
                        />
                    </FormGroup>
                </div>
                <div className="d-flex">
                    <FormGroup className="w-25 mr-1">
                        <Label>Capacity</Label>
                        <Input 
                            placeholder='Enter Capacity'
                            type='number'
                            onChange={(e)=>setCapacity(e.target.value)}
                            defaultValue={roomToEdit.capacity}
                        />
                    </FormGroup>
                    <FormGroup className="w-25 mx-1">
                        <Label>Room Area/Size</Label>
                        <Input 
                            placeholder='Room Area sqm.'
                            type='number'
                            onChange={(e)=>setArea(e.target.value)}
                            defaultValue={roomToEdit.area}
                        />
                    </FormGroup>
                    <FormGroup className="w-25 mx-1">
                        <Label>Bathrooms</Label>
                        <Input 
                            placeholder="No. of Bathrooms"
                            type='number'
                            onChange={(e)=>setBathrooms(e.target.value)}
                            defaultValue={roomToEdit.bathroom}
                        />
                    </FormGroup>
                    <FormGroup className="w-25 ml-1">
                        <Label>Beds</Label>
                        <Input 
                            placeholder='No. of Beds'
                            type='number'
                            onChange={(e)=>setBeds(e.target.value)}
                            defaultValue={roomToEdit.bed}
                        />
                    </FormGroup>
                </div>
                <FormGroup>
                    <Label for="exampleFile">File</Label>
                    <Input 
                        type="file" 
                        name="file" 
                        id="exampleFile"
                        onChange={selectMainImage}
                    />
                    <FormText color="muted">
                        Please select an image with good resolution.
                    </FormText>
                </FormGroup>
                <div className="p-4 mb-2 w-50 bg-secondary" style={{border:"1px dashed gray"}}>
                    <h4>Ameneties</h4>
                    <FormGroup check>
                        <Label check>
                        <Input 
                            type="checkbox" 
                            onClick={()=>setAircon(!aircon)}
                        />{' '}
                            Air Conditioning
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input 
                            type="checkbox"
                            onClick={()=>setShower(!shower)} 
                        />{' '}
                            Shower
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input 
                            type="checkbox" 
                            onClick={()=>setTelevision(!television)}
                        />{' '}
                            Televesion with Cable
                        </Label>
                    </FormGroup>
                    <FormGroup check>
                        <Label check>
                        <Input 
                            type="checkbox" 
                            onClick={()=>setWifi(!wifi)}
                        />{' '}
                            Wireless Internet Connection
                        </Label>
                    </FormGroup>
                </div>
            
                {isEditing
                ?
                    <Button color="primary" onClick={handleUpdateRoom}>Update</Button>
                :
                    <Button color="primary" onClick={handleSaveRoom}>Save</Button>
                }
            </ModalBody>
        </Modal>
    );
}
 
export default RoomForm;