import React from 'react';
import {Line} from 'react-chartjs-2';
const DailySalesChart = ({chartData,total,countTransactions,countGuest}) => {
    return (  
        <div className="row m-5">
            <div className="col-lg-8 col-sm-12">
                <Line 
                    type="line"
                    data={chartData} 
                    poin
                    options={{
                        responsive:true,
                        title:{text: 'Daily Sales Chart',display:true},
                        scales:{
                            yAxes: [
                                {
                                    ticks:{
                                        autoSkip: true,
                                        maxTicksLimit: 10,
                                        beginAtZero: true,
                                    },
                                    gridLines: {display:true}
                                }
                            ],
                            xAxes: [
                                {
                                    gridLines: {display:false}
                                }
                            ]
                        }
                    }}
                    
                />
            </div>
            <div className="col-lg-4 col-sm-12 p-3 d-flex flex-wrap align-items-center justify-content-center">
                <div className="card text-light bg-success mb-1" style={{minWidth: "20rem"}}>
                    <div className="card-body">
                        <h4 className="card-title">Total Sales</h4>
                        <h1 className="card-title">₱{total}</h1>
                    </div>
                </div>
                <div className="card text-light bg-info mb-1" style={{minWidth: "20rem"}}>
                    <div className="card-body">
                        <h4 className="card-title">Transactions</h4>
                        <h1 className="card-title">{countTransactions}</h1>
                    </div>
                </div>
                <div className="card text-light bg-warning mb-1" style={{minWidth: "20rem"}}>
                    <div className="card-body">
                        <h4 className="card-title">New Accounts</h4>
                        <h1 className="card-title">{countGuest}</h1>
                    </div>
                </div>
            </div>
        </div>
    );
}
 
export default DailySalesChart;