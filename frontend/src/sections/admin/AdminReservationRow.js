import React, { useState } from 'react';
import moment from 'moment';
import ReservationDetailsModal from './ReservationDetailsModal';
const AdminReservationRow = ({reservation,cancelReservation}) => {
    const [showModal,setShowModal] = useState(false);

    return ( 
        <> 
        <tr>
            <td>{moment(reservation.createdAt).startOf('day').fromNow()}</td>
            <td>{reservation.userId.fullname}</td>
            <td>{moment(reservation.startDate).format("MMM Do YY")}</td>
            <td>{moment(reservation.endDate).format("MMM Do YY")}</td>
            <td>{reservation.rooms.map(room=>(
                <span key={room.roomId._id} className="badge badge-info m-2">{room.roomId.name}-{room.count}</span>
            ))}</td>
            <td>{reservation.total}</td>
            <td>{reservation.status}</td>
            <td>
                <button onClick={()=>setShowModal(true)} className="btn btn-info">Details</button>
                {reservation.status === "Paid" &&
                    <button onClick={()=>cancelReservation(reservation._id)} className="btn btn-danger">Cancel</button>
                }
            </td>
        </tr>
        <ReservationDetailsModal reservation={reservation} showModal={showModal} toggleModal={()=>setShowModal(false)}/>
        </>
    );
}
 
export default AdminReservationRow;