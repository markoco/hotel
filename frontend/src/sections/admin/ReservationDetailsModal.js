import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import moment from 'moment'
const ReservationDetailsModal = ({showModal,toggleModal,reservation}) => {
    return (  
        <Modal className="modal-lg" isOpen={showModal} toggle={toggleModal}>
            <ModalHeader className="bg-primary" toggle={toggleModal}>
                <h4 className="text-light">Reservation Details</h4>
            </ModalHeader>
            <ModalBody>
                <h6>Guest: {reservation.userId.fullname}</h6>
                <h6>Check in: {moment(reservation.startDate).format('YYYY-MM-DD')}</h6>
                <h6>Check out: {moment(reservation.endDate).format('YYYY-MM-DD')}</h6>
                <h6>Total Amount: {reservation.total}{" "}{reservation.status}</h6>
                <h5 className="pt-3">Rooms:</h5>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Room Type</th>
                            <th>Rate/day</th>
                            <th>No. of rooms</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {reservation.rooms.map(room=>(
                            <tr key={room._id}>
                                <td>{room.roomId.name}</td>
                                <td>{room.roomId.price}</td>
                                <td>{room.count}</td>
                                <td>{room.roomId.price * room.count}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </ModalBody>
        </Modal>
    );
}
 
export default ReservationDetailsModal;