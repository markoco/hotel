import React, { useState } from 'react';
import RoomDetailsModal from '../rooms/RoomDetailsModal';
import swal from 'sweetalert';

const AdminRoomRow = ({room,editRoom,handleDeleteRoom}) => {
    const[showForm,setShowForm] = useState(false);
    return (  
        <>
            <div key={room._id} className="col-lg-4 p-0 m-0 b position-relative" style={{height:"70vh", backgroundImage:'url()'}}>
                <div className="d-flex flex-column align-items-start p-5 text-light justify-content-center" style={{position:"absolute", width:"100%", height:"100%",backgroundColor:"rgba(0,0,0,0.5)"}}>
                    <h5 className="text-uppercase text-light" style={{letterSpacing:"3px", borderBottom:"1px solid white"}}>{room.name}</h5>
                    <hr></hr>
                    <h6 className="text-light">Rate: ₱{room.price}</h6>
                    <h6 className="text-light">Capacity: {room.capacity}</h6>
                    <h6 className="text-light">Beds: {room.bed}</h6>
                    <h6 className="text-primary bg-light px-2 py-3 mt-3">{room.count} Room(s)</h6>
                    <u className="mt-5" onClick={()=>setShowForm(true)}><a style={{cursor:"pointer"}}>View all details</a></u>
                </div>
                <img src={'https://damp-woodland-92817.herokuapp.com/' + room.imgPath } alt="room image" width="100%" height="100%"/>
                <RoomDetailsModal showForm={showForm} toggleForm={()=>setShowForm(false)} room={room} />
                <div className="py-3" style={{position:"absolute", bottom: 0, right: 0, zIndex:5}}>
                    <u onClick={()=>editRoom(room)} className="px-3 text-light" style={{cursor:"pointer"}}>Edit</u>
                    <u onClick={()=>handleDeleteRoom(room._id)} className="px-3 text-light" style={{cursor:"pointer"}}>Delete</u>
                </div>
            </div>
        </>
    );
}
 
export default AdminRoomRow;