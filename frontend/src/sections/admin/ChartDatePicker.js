import React, {Component} from 'react';
import 'react-dates/initialize';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'

class ChartDatePicker extends Component {
  constructor(props){
    super(props);
    this.state ={
      startDate: null,
      endDate: null
    }
  }

  filterData = () =>{
    this.props.filterData(this.state.startDate,this.state.endDate)
  }

  render(){
    return (
      <div>
        <DateRangePicker
          startDate={this.state.startDate} // momentPropTypes.momentObj or null,
          startDateId="your_unique_start_date_id" // PropTypes.string.isRequired,
          endDate={this.state.endDate} // momentPropTypes.momentObj or null,
          endDateId="your_unique_end_date_id" // PropTypes.string.isRequired,
          onDatesChange={({ startDate, endDate }) => this.setState({ startDate, endDate })} // PropTypes.func.isRequired,
          focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
          onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
          startDatePlaceholderText= "From"
          endDatePlaceholderText= "To"
          isOutsideRange={day => (moment().diff(day) < 0)}
        />
        <button onClick={this.filterData} className="btn btn-info">Filter Data</button>
      </div>
    );
  }
}

export default ChartDatePicker;
