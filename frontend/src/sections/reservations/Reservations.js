import React, {useEffect,useState} from 'react';
import ReservationRow from './ReservationRow';
import ReactPaginate from 'react-paginate';
const Reservations = () => {
    const[reservations,setReservations] = useState([])
    const [pages,setPages] = useState(0);
    const [initialPage, setInitialPage] = useState(0);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(()=>{
        setIsLoading(true)
        fetch('https://damp-woodland-92817.herokuapp.com/reservations/'+sessionStorage.userId+'/'+initialPage)
        .then(res => res.json())
        .then(res =>{
            let page = 0;
            if(res[0].length % 10 == 0){
                page = res[0].length/10
            }else{
                page = Math.floor(res[0].length /10) +1
            }
            setReservations(res[1]);
            setIsLoading(false)
            setPages(page);       			
        })
    },[]);

    const handlePaginate = (data) =>{
        setIsLoading(true);
        setInitialPage(data.selected);

        fetch('https://damp-woodland-92817.herokuapp.com/reservations/'+sessionStorage.userId+'/'+initialPage)
        .then(res => res.json())
        .then(res =>{
            setReservations(res[1]);
            setIsLoading(false);
        },[])
    }
    return (  
        <>
            <table className="table">
                <thead>
                    <tr>
                        <th>Reservation ID</th>
                        <th>Created At</th> 
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Rooms</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        isLoading
                        ?<div style={{height:"60vh", width:"100vw"}} className="d-flex align-items-center justify-content-center">
                            <p>Please wait...</p>
                        </div>
                        :reservations.map(reservation=>(
                            <ReservationRow key={reservation._id} reservation={reservation}/>
                        ))
                        
                    }
                </tbody>
            </table>
            <ReactPaginate
                previousLabel = {'<'}
                nextLabel = {'>'}
                marginPagesDisplayed = {5}
                breakClassName={'page-item'}
                breakLinkClassName = {'page-link'}
                containerClassName = {'pagination'}
                pageClassName ={'page-item'}
                pageLinkClassName = {'page-link'}
                previousClassName = {'page-item'}
                previousLinkClassName={'page-link'}
                nextClassName={'page-item'}
                nextLinkClassName={'page-link'}
                activeClassName={'active'}
                pageCount = {pages}
                onPageChange = {handlePaginate}
            /> 
        </>
    );
}
 
export default Reservations;