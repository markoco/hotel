import React, {useState, useEffect} from 'react';
import {Document,Page, View, Text, StyleSheet, PDFDownloadLink} from '@react-pdf/renderer';
import {Button} from 'reactstrap';
const styles = StyleSheet.create({
    header:{
        height: '200px',
        width: '100%',
        textAlign: 'center'
    },
    brandName: {
        fontSize: 40,
        fontWeight: 'bold',
        color: 'black',
        textAlign: 'center'
    },
    detailsContainer:{
        display: 'flex',
        justifyContent: 'center'
    },
    details:{
        margin: '5% 20%'
    },
    detailsText:{
        color: 'darkgray',
        textAlign: 'center'
    }
})
const Ticket = ({reservation})=>{
    return(
        <Document>
            <Page size='A4'>
                <View
                    style={styles.header}
                >
                    <Text style={styles.brandName}></Text>
                </View>
                <View style={styles.detailsContainer}>
                    <View style={styles.details}>
                        <Text style={styles.brandName}>Calaguas Hotel Reservation Receipt</Text>
                        <Text style={styles.detailsText}>Guest Name: {reservation._id} </Text>
                        <Text style={styles.detailsText}>Guest Name: {sessionStorage.fullname} </Text>
                        <Text style={styles.detailsText}>Date of reservation: {reservation.createdAt} </Text>
                        <Text style={styles.detailsText}>Check in: {reservation.startDate} </Text>
                        <Text style={styles.detailsText}>Check out: {reservation.endDate} </Text>
                        <Text style={styles.detailsText}>Amount: {reservation.total} </Text>
                        <Text style={styles.detailsText}>Status: {reservation.status} </Text>
                    </View>
                    <View style={styles.details}>
                        <Text style={styles.detailsText}>This receipt is non-refundable</Text>
                    </View>
                </View>
            </Page>
        </Document>
    )
}

const DownloadReceipt = ({reservation}) =>{
    const [open,setOpen] = useState(false);

    useEffect(()=> {
        setOpen(false);
        setOpen(true);
        return () => setOpen(false)
    })
    return(
        <>
            {open && 
            <PDFDownloadLink
                document = {<Ticket reservation={reservation}/>}
                fileName = {"Ticket#"+Date.now()+".pdf"}
            >
                {({loading})=> loading? "Loading...":"Receipt"}
            </PDFDownloadLink>
}
        </>
        
    )
}

export default DownloadReceipt