import React from 'react';
import moment from 'moment'
import DownloadReceipt from './DownloadReceipt';
const ReservationRow = ({reservation}) => {
    return (  
        <tr>
            <td>{reservation._id}</td>
            <td>{moment(reservation.createdAt).format('YYYY-MM-DD')}</td>
            <td>{moment(reservation.startDate).format('YYYY-MM-DD')}</td>
            <td>{moment(reservation.endDate).format('YYYY-MM-DD')}</td>
            <td>{reservation.rooms.map(room=>(
                <span className="badge badge-success m-1">{room.roomId.name} - {room.count}</span>
            ))}
            </td>
            <td>{reservation.total}</td>
            <td>{reservation.status}</td>
            <td>
                {reservation.status === 'Paid'&&
                    <DownloadReceipt reservation={reservation}/>
                }
            </td>
            
        </tr>
    );
}
 
export default ReservationRow;