import React, { useState,useEffect, useCallback } from 'react';
import DatePicker from './DatePicker';
import RoomRow from './RoomRow';
import moment from 'moment';
import { ErrorToast,SuccessToast } from '../../components/Toasts';
import PaymentModal from './PaymentModal';
import {Elements} from '@stripe/react-stripe-js';
import {loadStripe} from '@stripe/stripe-js';
const stripePromise = loadStripe('pk_test_LvkVZjMIn5cmDQ0gA0rvb5On00m8VrFSnI');

const Rooms = () => {
    const[rooms,setRooms] = useState([]);
    const[reservedRooms,setReservedRooms] = useState([]);
    const[newRoomCount,setNewRoomCount] = useState([]);
    const[startDate,setStartDate] = useState(null);
    const[endDate,setEndDate] = useState(null);
    const[selectedRooms,setSelectedRooms] = useState([]);
    const[selectedRoomCount,setSelectedRoomCount] = useState(0);
    const[total,setTotal] = useState(0);
    const [showPaymentForm,setShowPaymentForm] = useState(false);
    const[days,setDays] = useState(0);
    useEffect(()=>{
        fetch('https://damp-woodland-92817.herokuapp.com/admin/rooms')
        .then(res => res.json())
        .then(res =>{
            setRooms(res);
            setNewRoomCount(res);
        })     
    },[]);

    const getRooms = () =>{
        fetch('https://damp-woodland-92817.herokuapp.com/admin/rooms')
        .then(res => res.json())
        .then(res =>{
            setRooms(res);
        })
    }

    const searchRooms = (sDate,eDate) =>{
        if(sDate && eDate){
            getRooms();
            setStartDate((sDate).format("YYYY-MM-DD"));
            setEndDate((eDate).format("YYYY-MM-DD"));
            let s = moment([sDate.format("YYYY"),sDate.format("MM"),sDate.format("DD")]);
            let e = moment([eDate.format("YYYY"),eDate.format("MM"),eDate.format("DD")]);
            setDays(moment(e).diff(s, 'days'));

            let apiOptions = {
                method: "POST",
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    startDate: sDate,
                    endDate: eDate
                })
            }

            fetch('https://damp-woodland-92817.herokuapp.com/searchrooms', apiOptions)
            .then(res => res.json())
            .then(res => {
                let searchedReservedRooms = [];
                res.map(indivRoom=>{
                return indivRoom.rooms.map(room=>{
                    let roomId = room.roomId;
                    let count = room.count;
                    searchedReservedRooms = [...searchedReservedRooms,{"id":roomId,"count":count}]
                    })
                })  
                setReservedRooms(searchedReservedRooms);
                updateRoomCount(searchedReservedRooms);
                SuccessToast('Available rooms from '+(sDate).format("YYYY-MM-DD")+" to "+(eDate).format("YYYY-MM-DD"));
            })
            .catch((e)=>{
                ErrorToast('Something Went Wrong Please try Again.');
            })
        }else{
            ErrorToast("Please select check In and check out date")
        }
        
    }

    const handleSetSelectedRooms = (selectedRoom) =>{
        const checkRoom = selectedRooms.filter(indivSelectedRoom => {
            return !indivSelectedRoom.room._id.includes(selectedRoom.room._id) || indivSelectedRoom.count === 0;
        });

        let roomCount = 0
        let totalAmount = 0
        if(selectedRoom.count !== 0){
            setSelectedRooms([...checkRoom,selectedRoom]);
            ([...checkRoom,selectedRoom]).forEach( data => {
                roomCount = roomCount + data.count;
                totalAmount = totalAmount +(data.room.price * data.count * days);
            });
        }else{
            setSelectedRooms(checkRoom)
            checkRoom.forEach( data => {
                roomCount = roomCount + data.count;
                totalAmount = totalAmount +(data.room.price * data.count * days);
            });
        }

        setSelectedRoomCount(roomCount);
        setTotal(totalAmount);
        
    }


    const updateRoomCount = (searchedReservedRooms) =>{
        const updatedRoomCount = rooms.map(indivRoom => {
            let newCount = indivRoom.count
            searchedReservedRooms.forEach(indivReservedRoom =>{
                if(indivReservedRoom.id === indivRoom._id){
                    newCount = newCount - indivReservedRoom.count
                }
            });
            indivRoom.count = newCount
            return indivRoom 
        })
        setNewRoomCount(updatedRoomCount);  
    }

    const clearState = () =>{
        setReservedRooms([]);
        setSelectedRooms([]);
        setSelectedRoomCount(0);
        setTotal(0);
        setDays(0);
        searchRooms(moment(startDate),moment(endDate));
    }

    const handleSetSowPaymentForm = () =>{
        if(selectedRooms.length > 0){
            setShowPaymentForm(true)
        }else{
            ErrorToast("You haven't selected any rooms yet.");
        }
        
    }
    
    return (  
        <>
            <div id="rooms" className="row py-3 bg-primary vw-100">
                <div className="col-lg-6 text-center">
                    <DatePicker searchRooms={searchRooms}/>
                </div>
                {
                sessionStorage.userId ?
                <div className="col-lg-6 my-2 d-flex align-items-center justify-content-center">
                    <div className="d-flex">
                        <h3 className="lead text-light">{selectedRoomCount}  Room(s) | </h3>
                        <h3 className="lead text-light"> ₱ {total} Total  </h3>
                    </div> 
                    <button className="mx-2 btn btn-info" onClick={handleSetSowPaymentForm}>Pay Via Stripe</button>
                </div>
                :<p className="m-auto">*Please login to book a room</p>
                }
            </div>
            
            <div className="row m-0 p-0 bg-primary" style={{minHeight:"100vh"}}>
                {newRoomCount.map(room=>(
                    <RoomRow key={room._id} room={room} startDate={startDate} endDate={endDate} handleSetSelectedRooms={handleSetSelectedRooms}/>
                ))}
            </div>
            <Elements stripe = {stripePromise}>
                <PaymentModal 
                    showPaymentForm={showPaymentForm} 
                    toggleForm={()=>setShowPaymentForm(false)}
                    setShowPaymentForm={setShowPaymentForm}
                    total={total}
                    startDate={startDate}
                    endDate={endDate}
                    selectedRooms={selectedRooms}
                    days={days}
                    clearState={clearState}
                />
            </Elements>
        </>
    );
}
 
export default Rooms;