import React, { useState } from 'react';
import RoomDetailsModal from './RoomDetailsModal';

const RoomRow = ({room,handleSetSelectedRooms,startDate,endDate}) => {
    const[showForm,setShowForm] = useState(false);

    const handleChooseRoom = (e,room )=>{
        const reservedRoomDetails = {"room":room,"count":parseInt(e.target.value)}
        handleSetSelectedRooms(reservedRoomDetails);
    }

 

    return (  
        <>
            {room.count > 0 &&
                <div key={room._id} className="col-lg-4 p-0 m-0 b position-relative" style={{height:"70vh", backgroundImage:'url()'}}>
                    <div className="d-flex flex-column align-items-start p-5 text-light justify-content-center" style={{position:"absolute", width:"100%", height:"100%",backgroundColor:"rgba(0,0,0,0.5)"}}>
                        <h5 className="text-uppercase text-light" style={{letterSpacing:"3px", borderBottom:"1px solid white"}}>{room.name}</h5>
                        <hr></hr>
                        <h6 className="text-light">Rate: ₱{room.price}</h6>
                        <h6 className="text-light">Capacity: {room.capacity}</h6>
                        <h6 className="text-light">Beds: {room.bed}</h6>
                        {
                        startDate !== null && endDate !== null && sessionStorage.userId?
                            <div className="form-group mt-3">
                                <select className="custom-select" onChange={(e)=>handleChooseRoom(e,room)}>
                                    { [...Array(room.count+1)].map((u, i) => 
                                        <option key={i} value={i}>{i} Room(s) Selected</option>
                                    )}
                                </select>
                            </div> 
                        :
                            <h6 className="text-primary bg-light px-2 py-3 mt-3">{room.count} Available Room(s)</h6>
                        }
                        
                        <u className="mt-5" onClick={()=>setShowForm(true)}><a style={{cursor:"pointer"}}>View all details</a></u>
                    </div>
                    <img src={'https://damp-woodland-92817.herokuapp.com/' + room.imgPath } alt="room image" width="100%" height="100%"/>
                    <RoomDetailsModal showForm={showForm} toggleForm={()=>setShowForm(false)} room={room} />
                </div>
            }
        </>
    );
}
 
export default RoomRow;