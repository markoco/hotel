import React from 'react';
import { Modal, ModalHeader, ModalBody,Button, FormGroup } from 'reactstrap';
import { CardElement, useStripe, useElements} from '@stripe/react-stripe-js';
import { SuccessToast, ErrorToast } from '../../components/Toasts';

const PaymentModal = ({toggleForm,showPaymentForm,selectedRooms,total,setShowPaymentForm,startDate,endDate,days,clearState}) => {

    const stripe = useStripe();
    const elements = useElements();

    const saveCharge = async () =>{
        const cardElement = elements.getElement(CardElement);
        console.log(stripe);
        
        const {token} = await stripe.createToken(cardElement);

        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                token,
                email: sessionStorage.email,
                amount: total * 100 
            })
        }
        let paymentResponse = await fetch('https://damp-woodland-92817.herokuapp.com/charge', apiOptions);
        
        if(paymentResponse.ok){
            payment();
        }else{
                ErrorToast('Something went wrong. Please try again.');
        }

        setShowPaymentForm(false);
   }

   const payment = () =>{
    let roomIdAndCount = [];
    selectedRooms.forEach(indivSelectedRoom => {
        roomIdAndCount = [...roomIdAndCount,{"roomId":indivSelectedRoom.room._id,"count":indivSelectedRoom.count}]
    });
    let apiOptions = {
        method: "POST",
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
            userId: sessionStorage.userId,
            rooms: roomIdAndCount,
            startDate,
            endDate,
            status: "Paid",
            total
        })
    }
        fetch('https://damp-woodland-92817.herokuapp.com/addreservation', apiOptions)
        .then(res => res.json())
        .then(res => {
            SuccessToast('Thank you for booking. You may download the electronic receipt on reservations page.')
            clearState()
        })
        .catch((e)=>{
            ErrorToast(e)
        })
}


    return (  
        <Modal 
            className="modal-lg" 
            isOpen={showPaymentForm} 
            toggle={toggleForm}
        >
            <ModalHeader 
                className="bg-primary" 
                toggle={toggleForm}
            >
                <h4 className="text-light">Payment Summary</h4>
            </ModalHeader>
            <ModalBody>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Room</th>
                            <th>Rate/day/room</th>
                            <th># Room(s)</th>
                            <th>Days</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        {selectedRooms.map((indivSelectedRoom,index)=>(
                            <tr key={index}>
                                <td>{indivSelectedRoom.room.name}</td>
                                <td>{indivSelectedRoom.room.price}</td>
                                <td>{indivSelectedRoom.count}</td>
                                <td>{days}</td>
                                <td>{indivSelectedRoom.room.price * indivSelectedRoom.count * days}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div className="row">
                    <div className="col-lg-6">
                        <p>Check In: {startDate}</p>
                        <p>Check Out: {endDate}</p>
                    </div>
                    <div className="col-lg-6 text-right">
                        <div className="d-flex align-items-center justify-content-end"> 
                            <h5>Amount:</h5>
                            <h1>₱{total}</h1>
                        </div>
                        <FormGroup className="my-3">
                            <div className="card p-3 my-3">
                                <CardElement />
                            </div>

                            <img src="/images/stripe.png" width="200px"/>
                            
                            <div className="p-3">
                                * Note: You may use the test accounts provided by 
                                <a className="mx-1" href="https://stripe.com/docs/testing" target="_blank">Stripe</a> 
                            </div>
                        
                            
                            <Button 
                                color='info'
                                disabled={!stripe}
                                onClick ={saveCharge}
                                className='mt-3'
                            >
                                Pay
                            </Button>
                        </FormGroup>
                    </div>
                </div>
            </ModalBody>
        </Modal>
    );
}
 
export default PaymentModal;