import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
const RoomDetailsModal = ({showForm,toggleForm,room}) => {
    return (  
        <Modal 
            isOpen={showForm}
            toggle={toggleForm}
            className="modal-lg"
        >
            <ModalHeader className="bg-primary" toggle={toggleForm}>
                <h3 className="text-light">{room.name} {' '} ₱{room.price}</h3>
            </ModalHeader>
            <ModalBody className="d-flex justifty-content-start align-items-start">
                <div className="row">
                    <div className="col-lg-6 mb-4">
                        <img src={'https://damp-woodland-92817.herokuapp.com/' + room.imgPath } width="100%" height="350px"/>
                    </div>
                    <div className="col-lg-6 px-3" style={{fontSize:"12px"}}>
                        <h4>Room Details</h4>
                        <p>{room.description}</p>
                        <div className="mr-2" style={{fontSize: "12px"}}>
                            <b>Beds: </b>
                            <span>{room.bed}</span>
                        </div>
                        <div className="mr-2" style={{fontSize: "12px"}}>
                            <b>Capacity: </b>
                            <span>{room.capacity}</span>
                        </div>
                        <div className="mr-2" style={{fontSize: "12px"}}>
                            <b>Area: </b>
                            <span>{room.area}m²</span>
                        </div>
                        <hr></hr>
                        <h6>Amenities</h6>
                        <div className="mr-2" style={{fontSize: "12px"}}>
                        {
                            room.shower &&
                            <div className="d-flex">
                                <img src="/images/shower.png" height="15px" className="m-1"/>
                                <p>Batroom and Shower</p>
                            </div>
                        }
                        {
                            room.aircon &&
                            <div className="d-flex">
                                <img src="/images/air-conditioner.png" height="15px" className="mx-1"/>
                                <p>Air Conditioning</p>
                            </div>
                        }
                        {
                            room.television &&
                            <div className="d-flex">
                                <img src="/images/television.png" height="15px" className="mx-1"/>
                                <p>Television with Cable</p>
                            </div>
                        }
                        {
                            room.wifi &&
                            <div className="d-flex">
                                <img src="/images/wifi.png" height="15px" className="mx-1"/>
                                <p>Wireless Internet Connection</p>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </ModalBody>
        </Modal>
    );
}
 
export default RoomDetailsModal;