import React from 'react';
import Slideshow from './Slideshow';
import Navbar from '../../components/navbar/Navbar';

const Home = () => {
    return (  
        <div className="position-relative">
            <Navbar/>
            <Slideshow/>
        </div>
    );
}
 
export default Home;