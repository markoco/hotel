import React from 'react';
import { Zoom } from 'react-slideshow-image';
 
const images = [
  'images/hotel1.jpg',
  'images/hotel2.jpg',
  'images/hotel3.jpg',
  'images/hotel4.jpg',
];
 
const zoomOutProperties = {
  duration: 5000,
  transitionDuration: 500,
  infinite: true,
  indicators: false,
  scale: 0.2,
  arrows: true
}
 
const Slideshow = () => {
    return (
      <div className="slide-container">
        <div 
          className="px-5 d-flex flex-column justify-content-center align-items-center" 
          style={{
            width:"100%",
            height:"100%",
            position:"absolute", 
            background: "rgba(0, 0, 0, 0.7)",
            zIndex:2
          }}
        >
          <h1 
              className="text-light text-left py-2"
              style={{
                fontSize:'2rem', 
                fontWeight:"bolder"
              }}
          >
              Calaguas Resort
          </h1>
          <h3 
            className="lead text-light text-left" 
            style={{fontSize:".8rem"}}
          >
            The energetic Calaguas resort for the adventurer in you
          </h3>
          <a 
            href="#rooms" 
            className="btn px-5 my-4 text-white" 
            style={{backgroundColor:"black"}}
          >
            Book Now
          </a>
        </div>
        <Zoom {...zoomOutProperties}>
          {
            images.map((each, index) => 
              <div 
                key={index}
                style={{
                  minWidth:"100vw",
                  height:"100vh",
                  backgroundPosition:"center",
                  backgroundSize:"cover",
                  backgroundRepeat:"no-repeat", 
                  backgroundImage: "url(" + each + ")"}}
              >
              </div>)
          }
        </Zoom>
      </div>
    )
}

export default Slideshow;