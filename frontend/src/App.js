import React from 'react';
import {
  BrowserRouter, 
  Route, 
  Switch
} from 'react-router-dom';

function App() {
  const Home = React.lazy(() => import('./pages/LandingPage'));
  const Login = React.lazy(() => import('./pages/Login'));
  const Register = React.lazy(() => import('./pages/Register'));
  const Reservations = React.lazy(() => import('./pages/GuestReservationsPage'));
  const AdminHome = React.lazy(() => import('./pages/AdminHome'));
  const AdminReservations = React.lazy(() => import('./pages/AdminReservations'));
  const AdminRooms = React.lazy(() => import('./pages/AdminRooms'));
  return (
    <BrowserRouter>
      <React.Suspense
          fallback={""}
      >
          <Switch>
              <Route
                  path="/"
                  exact
                  render={props => 
                    !sessionStorage.userId || sessionStorage.isAdmin==="false"
										?<Home{...props} /> 
										:<Login{...props} />
                  } 
              />
              <Route
                  path="/login"
                  render={props => <Login{...props} />}
              />
              <Route
                  path="/register"
                  render={props => <Register{...props} />}
              />
              <Route
                  path="/reservations"
									render={props => 
										sessionStorage.userId && sessionStorage.isAdmin==="false"
										?<Reservations{...props} /> 
										:<Login{...props} />
									}
              />
              <Route
                  path="/adminhome"
									render={props =>  
										sessionStorage.userId && sessionStorage.isAdmin==="true" 
										?<AdminHome{...props} /> 
										:<Login{...props} />
									}
              />
              <Route
                  path="/adminreservations"
									render={props => 
										sessionStorage.userId && sessionStorage.isAdmin==="true" 
										?<AdminReservations{...props} /> 
										:<Login{...props} />
									}
              />
              <Route
                  path="/adminrooms"
									render={props => 
										sessionStorage.userId && sessionStorage.isAdmin==="true" 
										?<AdminRooms{...props} /> 
										:<Login{...props} />
									}
              />
          </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
}

export default App;
