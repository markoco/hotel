import React from 'react';
import { Modal, ModalHeader,ModalBody } from 'reactstrap';

const ContactModal = ({showForm,toggleForm}) => {
    return (  
        <Modal isOpen={showForm} toggle={toggleForm}>
            <ModalHeader className="bg-primary" toggle={toggleForm}>
                <h4 className="text-light">Contact Us</h4>
            </ModalHeader>
            <ModalBody className="text-center">
                <p>Phone Number: +63929297979 (SMART)</p>
                <p>Telephone: 889 887 889</p>
                <p>Address: Caluguas, Vinzons, Camarines Norte</p>
            </ModalBody>
        </Modal>
    );
}
 
export default ContactModal;