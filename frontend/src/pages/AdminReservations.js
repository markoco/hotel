import React, {useEffect, useState} from 'react';
import Navbar from '../components/navbar/Navbar';
import AdminReservationRow from '../sections/admin/AdminReservationRow';
import ReactPaginate from 'react-paginate';
import { CSVLink } from 'react-csv';
import swal from 'sweetalert';
import { DeleteToast, SuccessToast } from '../components/Toasts';
import Footer from '../components/Footer';

const AdminReservations = () => {
    const [reservations,setReservations] = useState([]);
    const [pages,setPages] = useState(0);
    const [initialPage, setInitialPage] = useState(0);

    const reservationData = [
        ['ID', 'Guest', 'Check In', 'check Out', 'Amount', 'Status']
    ]

    reservations.forEach( indivReservation => {
        let indivReservationData = [];
        indivReservationData.push(indivReservation._id);
        indivReservationData.push(indivReservation.userId.fullname);
        indivReservationData.push(indivReservation.startDate);
        indivReservationData.push(indivReservation.endDate);
        indivReservationData.push(indivReservation.total);
        indivReservationData.push(indivReservation.status);

        reservationData.push(indivReservationData);
    })
    
    useEffect(()=>{
        fetch('https://damp-woodland-92817.herokuapp.com/adminreservations/'+initialPage)
        .then(res => res.json())
        .then(res =>{
            setReservations(res[1]);
            let page = 0;
            if(res[0].length % 10 == 0){
                page = res[0].length/10
            }else{
                page = Math.floor(res[0].length /10) +1
            }
            setPages(page);
        })
    },[]);

    const cancelReservation = (id) =>{
        swal({
            title: "Are you sure?",
            text: "Once canelled, you will not be able to revert it!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                const apiOptions = {
                    method: "PATCH",
                    headers: {'Content-type': 'application/json'},
                    body: JSON.stringify({
                        id
                    })
                }
        
               fetch('https://damp-woodland-92817.herokuapp.com/cancelreservation', apiOptions)
               .then(res => res.json())
               .then(res => {
                    let newReservations= reservations.map((indivReservation) => {
                        if (id === indivReservation._id) {
                        return res;
                        }
                        return indivReservation;
                    });
                    setReservations(newReservations);
                })
                SuccessToast("Reservation "+ id +" has been cancelled")
            }     
          });        
    }

    const handlePaginate = (data) =>{
        setInitialPage(data.selected);
        console.log("hey");
        
        fetch('https://damp-woodland-92817.herokuapp.com/adminreservations/'+data.selected)
        .then(res => res.json())
        .then(res =>{
            setReservations(res[1]);
        },[])
    }
    return (  
        <div className="position-relative">
            <div className="bg-primary" style={{height:"15vh"}}></div>
            <Navbar/>
            <div className="d-flex justify-content-between align-items-center p-5">
                <h1>Guests' Reservation</h1>
                <button
                    className="btn btn-success"
                  >
                    <CSVLink
                      data={reservationData}
                      filename = {'reservation_reports on ' + new Date() + '.csv'}
                    >Export to Excel</CSVLink>
                  </button>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Created At</th>
                        <th>Guest</th> 
                        <th>Check In</th>
                        <th>Check Out</th>
                        <th>Rooms</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {reservations.map(reservation=>(
                       <AdminReservationRow cancelReservation={cancelReservation} key={reservation._id} reservation={reservation} />
                    ))}
                </tbody>
            </table>
            <ReactPaginate
                previousLabel = {'<'}
                nextLabel = {'>'}
                marginPagesDisplayed = {5}
                breakClassName={'page-item'}
                breakLinkClassName = {'page-link'}
                containerClassName = {'pagination'}
                pageClassName ={'page-item'}
                pageLinkClassName = {'page-link'}
                previousClassName = {'page-item'}
                previousLinkClassName={'page-link'}
                nextClassName={'page-item'}
                nextLinkClassName={'page-link'}
                activeClassName={'active'}
                pageCount = {pages}
                onPageChange = {handlePaginate}
            />
            <Footer/>



        </div>
    );
}
 
export default AdminReservations;