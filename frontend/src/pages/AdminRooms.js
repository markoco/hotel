import React, { useEffect, useState } from 'react';
import Navbar from '../components/navbar/Navbar';
import AdminRoomRow from '../sections/admin/AdminRoomRow';
import RoomForm from '../sections/admin/RoomForm';
import { SuccessToast, ErrorToast, UpdateToast, DeleteToast } from '../components/Toasts';
import swal from 'sweetalert';
import Footer from '../components/Footer';

const AdminRooms = () => {
    const [rooms,setRooms] = useState([]);
    const [showForm,setShowForm] = useState(false);
    const [isEditing,setIsEditing] = useState(false);
    const [roomToEdit,setRoomToEdit] = useState({});
    useEffect(()=>{
        fetch('https://damp-woodland-92817.herokuapp.com/admin/rooms')
        .then(res => res.json())
        .then(res =>{
            setRooms(res);
        })
    },[]);

    const saveRoom = (name,description,price,count,capacity,area,bathroom,bed,imgPath,aircon,shower,television,wifi) =>{
        let apiOptions = {
            method: "POST",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                name,
                description,
                price,
                count,
                capacity,
                area,
                bathroom,
                bed,
                imgPath,
                aircon,
                shower,
                television,
                wifi
            })
        }
            fetch('https://damp-woodland-92817.herokuapp.com/admin/addroom', apiOptions)
            .then(res => res.json())
            .then(res => {
                setRooms([...rooms,res]);
                setShowForm(false);
                SuccessToast("A new room type has been successfully added")
            })
            .catch((e)=>{
                ErrorToast(e)
            })
    }

    
    const updateRoom = (id,name,description,price,count,capacity,area,bathroom,bed,imgPath,aircon,shower,television,wifi) =>{
       
        let apiOptions = {
            method: "PATCH",
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                id,
                name,
                description,
                price,
                count,
                capacity,
                area,
                bathroom,
                bed,
                imgPath,
                aircon,
                shower,
                television,
                wifi
            })
        }

      
            fetch('https://damp-woodland-92817.herokuapp.com/admin/updateroom', apiOptions)
            .then(res => res.json())
            .then(res => {
                let newRooms = rooms.map(room => {
                    if (room._id === id) {
                        return res
                    }
                    return room;
                });
                setRooms(newRooms);
                setShowForm(false);
                UpdateToast(name)
            })
            .catch((e)=>{
                console.log('error here');
            })
    }
    

    const handleDeleteRoom = (id) => {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {

                let apiOptions = {
                    method: "DELETE",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify({ id })
                }
        
                fetch('https://damp-woodland-92817.herokuapp.com/admin/deleteroom', apiOptions)
                .then(res => res.json())
                .then(res => {
                    const newRooms = rooms.filter(room => {
                        return room._id !== id
                    })
                    setRooms(newRooms);
                    DeleteToast(id)
                })
            }
          });
    }

    const editRoom = (room) => {
        setShowForm(true);
        setIsEditing(true);
        setRoomToEdit(room);
    };

    const addRoom = () =>{
        setShowForm(true);
        setIsEditing(false);
        setRoomToEdit({});
    }


    return (  
        <div className="position-relative">
            <div className="bg-primary" style={{height:"15vh"}}></div>
            <Navbar/>
            <div className="row m-0 p-0 bg-primary" style={{minHeight:"100vh"}}>
                <div 
                    onClick = {addRoom}
                    className="col-lg-4 p-0 m-0 d-flex flex-column align-items-center justify-content-center position-relative" 
                    style={{height:"70vh", backgroundImage:'url()', cursor:"pointer"}}
                >
                    <img src="/images/addroom.png" width="125px" height="125px"/>
                    <h3 className="text-secondary p-5">Add New Room Type</h3>
                </div>
                {rooms.map(room=>(
                    <AdminRoomRow 
                        key={room._id} 
                        room={room} 
                        editRoom={editRoom} 
                        handleDeleteRoom={handleDeleteRoom}
                    />
                ))}
            </div>
            <RoomForm 
                showForm = {showForm}
                toggleForm = {()=>setShowForm(false)}
                saveRoom = {saveRoom}
                updateRoom = {updateRoom}
                isEditing = {isEditing}
                roomToEdit  ={roomToEdit}
            />
            <Footer/>
        </div>
    );
}
 
export default AdminRooms;