import React, { useState, useEffect } from 'react';
import {FormGroup,Label,Input,Button} from 'reactstrap';
import NavbarLogin from '../components/navbar/NavbarLogin'
import Footer from '../components/Footer';
import { useHistory } from 'react-router-dom';
import { ErrorToast } from '../components/Toasts';

const Login = () => {
    const [email,setEmail] = useState('');
    const [password,sePassword] = useState('');
    const history = useHistory();

    useEffect(()=>{
        sessionStorage.clear();
    },[])

    const handleLogin = async() =>{

        const apiOptions = {
            method: "POST",
            headers: {'Content-type': 'application/json'},
            body: JSON.stringify({
                email,
                password
            })
        }

        fetch('https://damp-woodland-92817.herokuapp.com/login', apiOptions)
        .then(res => res.json())
        .then(res => {
            sessionStorage.token = res.token;
            sessionStorage.userId = res.user.id;
            sessionStorage.email = res.user.email;
            sessionStorage.isAdmin = res.user.isAdmin;
            sessionStorage.fullname = res.user.fullname ;
            
            sessionStorage.isAdmin == "true"
            ?
            history.push('/adminhome')
            :
            history.push('/')
        })
        .catch((error) => {
            ErrorToast("Invalid Email or Password");
        });
    }
    return (
        <div className="position-relative">
            <div style={{
                        backgroundImage:"url(/images/hotel6.jpg)",
                        backgroundSize:"cover",
                        backgroundRepeat: "no-repeat",
                        width: "100vw",
                        height: "100vh"
                    }}
            >
                
            </div>
            <div
                className='d-flex justify-content-center align-items-center vh-100 vw-100'
                style={{position:"absolute",top:0,background: "rgba(0, 0, 0, 0.5) "}}
            >
                <NavbarLogin/>
                <div className="col-lg-6 p-5 position-relative bg-light">
                    <h1 className="text-center">Login</h1>
                    <form>
                        <FormGroup>
                            <Label>Email:</Label>
                            <Input
                                type='email'
                                placeholder='Enter your email'
                                onChange={(e)=>setEmail(e.target.value)}
                            />
                        </FormGroup>
                        <FormGroup>
                            <label>Password:</label>
                            <Input
                                type='password'
                                placeholder='Enter your password'
                                onChange={(e)=>sePassword(e.target.value)}
                            />
                        </FormGroup>
                        <Button 
                            color="dark"
                            disabled = {email === '' || password === ''?true:false}
                            onClick={handleLogin}
                        >
                            Login
                        </Button>
                    </form>
                    <hr></hr>
                    <h6>Test Accounts</h6>
                    <div className="d-flex">
                        <div className="m-3">
                            <p>Admin</p>
                            <p className="my-1">Email: marioAdmin25@email.com</p>
                            <p className="my-1">Password: @RiesAdmin25</p>
                        </div>
                        <div className="m-3">
                            <p>User</p>
                            <p className="my-1">Email: marioOco23@gmail.com</p>
                            <p className="my-1">Password: @Ries25</p>
                        </div>
                    </div>
                </div>

            </div>
            <Footer />
        </div>
        
    );
}
 
export default Login;