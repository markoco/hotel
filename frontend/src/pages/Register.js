import React, { useState } from 'react';
import { FormGroup, Label, Input, Button } from 'reactstrap';
import Navbar from '../components/navbar/Navbar';
import { ErrorToast } from '../components/Toasts';
import { useHistory } from 'react-router-dom';
import Footer from '../components/Footer';

const Register = () => {
    const [fullname,setFullname] = useState('');
    const [email,setEmail] = useState('');
    const [phone,setPhone] = useState('');
    const [password,setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const history = useHistory();


    const handleRegister = async () =>{
        if(fullname!=='' && email!=='' && phone !== '' && password !== '' && password === confirmPassword){
            const apiOptions = {
                method: "POST",
                headers: {'Content-type': 'application/json'},
                body: JSON.stringify({
                    fullname,
                    email,
                    phone,
                    password
                })
            }
            await fetch('https://damp-woodland-92817.herokuapp.com/register', apiOptions);
            history.push('/login');
        }else{
            ErrorToast('Please complete all the required fields')
        }
    }
    return (  
        <div className="position-relative">
            <div style={{
                        backgroundImage:"url(/images/hotel6.jpg)",
                        backgroundSize:"cover",
                        backgroundRepeat: "no-repeat",
                        width: "100vw",
                        height: "100vh"
                    }}
            >
                
            </div>
            <div 
                className='d-flex justify-content-center align-items-center vw-100 vh-100'
                style={{position:"absolute",top:0,background: "rgba(0, 0, 0, 0.5) "}}
            >
                <Navbar />
                <div className="col-lg-6 mt-5 bg-light p-5">
                    <h2>Register</h2>
                    <FormGroup>
                        <Label>Fullname</Label>
                        <Input 
                            placeholder="Fullname"
                            onChange = {(e)=>setFullname(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Phone Number</Label>
                        <Input 
                            type="number"
                            placeholder="Phone Number"
                            onChange = {(e)=>setPhone(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Email</Label>
                        <Input 
                            type="email"
                            placeholder="Email"
                            onChange={(e)=>setEmail(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Password</Label>
                        <Input 
                            type="password"
                            placeholder="Password"
                            onChange={(e)=>setPassword(e.target.value)}
                        />
                    </FormGroup>
                    <FormGroup>
                        <Label>Confirm Password</Label>
                        <Input 
                            type="password"
                            placeholder="Confirm Password"
                            onChange={(e)=>setConfirmPassword(e.target.value)}
                        />
                    </FormGroup>
                    <Button color="primary" onClick={handleRegister}>Register</Button>
                </div>
            </div>
            <Footer/>
        </div>
    );
}
 
export default Register;