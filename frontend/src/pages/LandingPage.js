import React from 'react';
import Home from '../sections/home/Home';
import Rooms from '../sections/rooms/Rooms';
import Footer from '../components/Footer';


const LandingPage = () => {
    return (  
        <div className="position-relative">
            <Home />
            <Rooms />
            <Footer />
        </div>
    );
}
 
export default LandingPage;