import React, { useState, useEffect } from 'react';
import Navbar from '../components/navbar/Navbar';
import moment from 'moment';
import ChartDatePicker from '../sections/admin/ChartDatePicker';
import DailySalesChart from '../sections/admin/DailySalesChart';
import Footer from '../components/Footer';

const AdminHome = () => {
    const [chartData,setChartData] = useState({});
    const [total,setTotal] = useState(0);
    const [countTransactions, setCountTransactions] = useState(0);
    const [countGuest,setCountGuest] = useState(0);

    useEffect(()=>{
        let end = new Date().toISOString().slice(0, 10);
        let start = new Date().toISOString().slice(0, 10)
            start= moment(start).subtract(30, 'days').format('YYYY-MM-DD');
              
        filterData(start,end)
    },[])

    const filterData = (sDate,eDate) =>{
        let start = moment(sDate).format('YYYY-MM-DD');
        let end = moment(eDate).add(1, 'days').format('YYYY-MM-DD');
  
        fetch('https://damp-woodland-92817.herokuapp.com/sales/'+start+"/"+end)
        .then(res => res.json())
        .then(res =>{
            let arrayLabels = [];
            let arrayData = [];
            let arrayTotal = 0;
            
            res[0].forEach(data => {
              arrayLabels = [...arrayLabels,moment(data._id).format('YYYY-MM-DD')]
              arrayData = [...arrayData,data.total]
              arrayTotal = arrayTotal + data.total;
            });
   
            setTotal(arrayTotal);
            setCountTransactions(res[1]);
            setCountGuest(res[2]);
        
            
            setChartData({
                labels:arrayLabels,
                datasets: [{
                    label: 'Sales',
                    fill: false,
                    data: arrayData,
                    borderColor: 'rgba(36, 37, 42, 1)',
                    pointRadius: 5,
                    lineTension: 0,
                    borderWidth: 1
                }]
            })
        }) 
    }


    return (  
        <div className="position-relative">
            <div className="bg-primary" style={{height:"15vh"}}></div>
            <Navbar/>
            <div className="m-4 d-flex align-items-center justify-content-between">
                <h1>Calaguas Resort Sales Report</h1>
                <ChartDatePicker filterData={filterData}/>
            </div>
            <DailySalesChart countGuest={countGuest} countTransactions={countTransactions} total={total} chartData={chartData}/>
            <Footer/>
        </div>
    );
}
 
export default AdminHome;