import React from 'react';
import Reservations from '../sections/reservations/Reservations';
import Navbar from '../components/navbar/Navbar';
import Footer from '../components/Footer';


const GuestReservationsPage = () => {
    return (  
        <div className="position-relative" style={{minHeight:"100vh"}}>
            <div className="bg-primary" style={{height:"15vh"}}></div>
            <Navbar/>
            <Reservations/>
            <Footer />
        </div>
    );
}
 
export default GuestReservationsPage;