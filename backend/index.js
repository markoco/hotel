const express = require('express');
const app = express();

const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./src/config');
const multer = require('multer');

//tinuturo tayo sa tamang path like /public/images
const path = require('path'); 
//allow saving files in the public directory
app.use(express.static(path.join(__dirname, 'public')));

app.use(express.urlencoded({extended:false}));
app.use(express.json());
app.use(cors())

const databaseUrl = process.env.DATABASE_URL || 'mongodb+srv://admin:admin@cluster0-rvmxv.mongodb.net/capstone3?retryWrites=true&w=majority';

mongoose.connect(databaseUrl,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
.then(()=>{
    console.log('Database Connection Successfully Established');
});

const PORT = process.env.PORT || 4000;
app.listen(PORT,()=>{
    console.log(`Listening on PORT ${PORT}`);
});

const userRoute = require('./src/routes/UserRoute');
app.use('/', userRoute);

const amenetyRoute = require('./src/routes/AmenetyRoute');
app.use('/admin', amenetyRoute);

const roomRoute = require('./src/routes/RoomRoute');
app.use('/admin', roomRoute);

const reservationRoute = require('./src/routes/ReservationRoute');
app.use('/', reservationRoute);

const paymentRoute = require('./src/routes/PaymentRoute');
app.use('/', paymentRoute);

let storage = multer.diskStorage({
    destination: (req, file, cb) =>{
        cb(null, 'public/images/uploads');
    },
    filename: (req, file, callback) => {
        callback(null, Date.now() + ' ' + file.originalname);
    }
});

const upload = multer({storage});
app.post('/upload', upload.single('image'), (req,res)=>{
    if(req.file){
        res.json({imageUrl: `images/uploads/${req.file.filename}`})
    }else{
        res.status(401).send('Bad request')
    }
})