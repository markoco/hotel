const jwt = require('jsonwebtoken');
const config = require('../config');

module.exports = (req,res, next) =>{
    const token = req.body.token;

    if(!token) return res.status(401).send('Bad request');

    try{
        let payload = jwt.verify(token, config.secret);

        if(payload.isAdmin){
            next()
        }else{
            res.status(401).send('Unauthorized')
        }
    }catch(e){
        res.status(401).send('Unauthorized')
    }
}