const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RoomSchema = new Schema({
    name: String,
    description: String,
    price: Number,
    count: Number,
    capacity: Number,
    area: String,
    bathroom: Number,
    bed: Number,
    imgPath: String,
    aircon: Boolean,
    shower: Boolean,
    television: Boolean,
    wifi: Boolean
}, {
    timestamps: true
});

module.exports = mongoose.model("Room", RoomSchema);