const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AmenetySchema = new Schema({
    name: String,
    description: String,
    iconPath: String
}, {
    timestamps: true
});

module.exports = mongoose.model("Amenety", AmenetySchema);