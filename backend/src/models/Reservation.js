const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const ReservationSchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId, 
        ref: 'User'
    },
    rooms: [{
        roomId: {
            type: Schema.Types.ObjectId,
            ref: 'Room'
        },
        count: Number
    }],
    startDate: Date,
    endDate: Date,
    status: String,
    total: Number
}, {
    timestamps: true
});

module.exports = mongoose.model("Reservation", ReservationSchema);