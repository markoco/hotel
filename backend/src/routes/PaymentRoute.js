const express = require('express');

const PaymentRoute = express.Router();


const stripe = require('stripe')('sk_test_Y139SMBcR8OU4PGrklvoUJec00tHqddBik')
//sk_test_Y139SMBcR8OU4PGrklvoUJec00tHqddBik

PaymentRoute.post('/charge', async(req,res)=>{

    try{
        await stripe.customers.create({
            email: req.body.email,
            description: "Payment for room",
            source: "tok_visa"
        })
    
       let chargeResponse = await stripe.charges.create({
            amount: req.body.amount,
            currency: 'php',
            source: "tok_visa",
            description: 'Payment fo room'
        })

        res.send(chargeResponse)
    }catch(e){
        res.status(401).send('Bad Request')
        
    }
})

module.exports = PaymentRoute;