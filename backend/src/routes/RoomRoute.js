const express = require('express');
const RoomRouter = express.Router();
const RoomModel = require('../models/Room');

const isAdmin= require('../middlewares/isAdmin_middleware');
RoomRouter.post('/addroom', async(req,res)=>{
    let room = RoomModel({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        count: req.body.count,
        capacity: req.body.capacity,
        area: req.body.area,
        bathroom: req.body.bathroom,
        bed: req.body.bed,
        imgPath: req.body.imgPath,
        aircon: req.body.aircon,
        shower: req.body.shower,
        television: req.body.television,
        wifi : req.body.wifi
    });

    try {
        room = await room.save();
        res.send(room);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.');
    }
});

RoomRouter.get('/rooms', async(req,res)=>{
    try {
        const rooms = await RoomModel.find().populate('ameneties.amenetyId');
        res.send(rooms);  
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.');
    }
});


RoomRouter.delete('/deleteroom', async(req,res)=>{
    try {
        const room = await RoomModel.findByIdAndDelete(req.body.id);
        res.send(room);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.');
    }
});

RoomRouter.patch('/updateroom', async(req,res)=>{
    const updates = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        count: req.body.count,
        capacity: req.body.capacity,
        area: req.body.area,
        bathroom: req.body.bathroom,
        bed: req.body.bed,
        imgPath: req.body.imgPath,
        aircon: req.body.aircon,
        shower: req.body.shower,
        television: req.body.television,
        wifi : req.body.wifi
    }

    try{
        const rooms = await RoomModel.findByIdAndUpdate(req.body.id,updates,{new:true});
        res.send(rooms);
    }catch (e){
        res.status(401).send('An error occurred. Please try again.');
    }
})

module.exports = RoomRouter;