const express = require('express');
const UserRouter = express.Router();
const UserModel = require('../models/User');

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');

UserRouter.post('/register', async(req,res)=>{ 
    const salt = bcrypt.genSaltSync(10);
    const hashed = bcrypt.hashSync(req.body.password, salt);

    let user  = UserModel({
        fullname: req.body.fullname,
        email: req.body.email,
        phone: req.body.phone,
        password: hashed
    });

    try{
        user = await user.save();
        res.send(user);
    }catch(e){
        res.status(401).send('An error occured. Please try again later.');
    }
});

UserRouter.get('/users', async(req,res)=>{
    try{
        let users = await UserModel.find();
        res.send(users);
    }catch(e){
        res.status(401).send('An error occurred. Please trt again.');
    }
});

UserRouter.delete('/deleteuser', async(req,res)=>{
    try {
        let user = await UserModel.findByIdAndDelete(req.body.id);
        res.send(user);
    } catch (e) {
        res.status(401).send('Error occurred. Please try again.');
    }
});

UserRouter.patch('/updateuser', async(req,res)=>{
    try {
        let updates = {
            fullname: req.body.fullname,
            email: req.body.email,
            phone: req.body.phone
        }
        let user = await UserModel.findByIdAndUpdate(req.body.id, updates , {new:true});
        res.send(user);
    } catch (e) {   
        res.status(401).send('Error occurred. Please try again.');
    }

});

UserRouter.post('/login', async(req,res)=>{
    let user = await UserModel.findOne({email: req.body.email});

    if(!user) return res.status(401).send('Email is not yet registered.');

    const matched = await bcrypt.compare(req.body.password, user.password);

    if(!matched) return res.status(401).send("Incorrect Password");

    const token  = jwt.sign({
        email: user.email,
        id: user._id,
        isAdmin: user.isAdmin
    }, config.secret, {expiresIn: 1800000});

    const loggedInUser = {
        token: token,
        user:{
            fullname: user.fullname,
            email: user.email,
            phone: user.phone,
            isAdmin: user.isAdmin,
            id: user._id
        }
    }

    res.header('x-auth-token', token).send(loggedInUser);

})



module.exports = UserRouter;