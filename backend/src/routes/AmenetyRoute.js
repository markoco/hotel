const express = require('express');
const AmenetyRouter = express.Router();
const AmenetyModel = require('../models/Amenety');

AmenetyRouter.post('/addamenety', async(req,res)=>{
    let amenety = AmenetyModel({
        name: req.body.name,
        description: req.body.description,
        iconPath: req.body.iconPath
    });
    try {
        amenety = await amenety.save();
        res.send(amenety);
    } catch (e) {
        res.status(401).send('An error occured. Please try again later.');
    }
});


AmenetyRouter.get('/ameneties', async(req,res)=>{
    try {
        let ameneties = await AmenetyModel.find();
        res.send(ameneties);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.');
    }
});


AmenetyRouter.delete('/deleteamenety', async(req, res)=>{
    try {
        let amenety = await AmenetyModel.findByIdAndDelete(req.body.id);
        res.send(amenety);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.')
    }
});

AmenetyRouter.patch('/updateamenety', async(req,res)=>{
    try {
        const updates = {
            name: req.body.name,
            description: req.body.description,
            iconPath: req.body.icon
        }

        let amenety = await AmenetyModel.findByIdAndUpdate(req.body.id,updates,{new:true});
        res.send(amenety);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again.');
    }
})

module.exports = AmenetyRouter;