const express = require('express');
const ReservationRoute =  express.Router();
const ReservationModel = require('../models/Reservation');
const UserModel = require('../models/User');
const moment = require('moment');
const auth = require('../middlewares/auth_middleware');


ReservationRoute.post('/addreservation', async(req,res)=>{
    try {
        let reservation = ReservationModel({
            userId: req.body.userId,
            rooms: req.body.rooms,
            startDate: moment(req.body.startDate),
            endDate:  moment(req.body.endDate),
            status: req.body.status,
            total: req.body.total
        });
    
        reservation = await reservation.save();
        res.send(reservation);
    } catch (e) {
       res.status(401).send('An error occurred. Please try again.') 
    }
});

ReservationRoute.get('/adminreservations/:offset', async(req,res)=>{
    try {
        const reservations = await ReservationModel.find();
        const selectedReservations = await ReservationModel.find().populate([{path:'rooms.roomId'},{path:'userId'}]).sort({createdAt: -1}).skip(parseInt(req.params.offset)*10).limit(10);
        res.send([reservations,selectedReservations]);
    } catch (e) {
        res.status(401).send('An error occured. Please try again.');
    }
});

ReservationRoute.get('/reservations/:id/:offset', async(req,res)=>{
    try {
        const reservations = await ReservationModel.find({userId: req.params.id}).populate('rooms.roomId');
        const selectedReservations = await ReservationModel.find({userId: req.params.id}).populate('rooms.roomId').skip(parseInt(req.params.offset)*10).limit(10)
        res.send([reservations,selectedReservations]);
    } catch (e) {
        res.status(401).send('An error occured. Please try again.');
    }
});

ReservationRoute.delete('/deletereservation', async(req,res)=>{
    try {
        const reservation = await ReservationModel.findByIdAndDelete(req.body.id);
        res.send(reservation);
    } catch (e) {
        res.status(401).send('An error occurred. Please try again');
    }
});

ReservationRoute.post('/searchrooms', async (req,res) =>{
    try{
        const reservations = await ReservationModel.find({$or:
            [{"startDate":{$gte:moment(req.body.startDate),$lte:moment(req.body.endDate)}},
            {"endDate":{$gte:moment(req.body.startDate),$lte:moment(req.body.endDate)}}]
        });
        res.send(reservations);
    }catch(e){
        res.status(401).send("An error occurred")
    }
});

ReservationRoute.get('/sales/:startDate/:endDate', async (req,res)=>{
    try {
        const sales = await ReservationModel.aggregate([
            { $match: { "createdAt": { $gte:new Date(req.params.startDate),$lt:new Date(req.params.endDate) } } },
            { $group : { _id :  {'$dateToString': {format: '%Y-%m-%d', date: '$createdAt'}},
             total: { $sum: "$total" } } },
            { $sort: { "_id": 1 } }
        ]);

        const reservationsCount = await ReservationModel.countDocuments({
            "createdAt":{
                $gte:moment(req.params.startDate),
                $lte:moment(req.params.endDate)
            }
        });

        const guestCount = await UserModel.countDocuments({
            "createdAt":{
                $gte:moment(req.params.startDate),
                $lte:moment(req.params.endDate)
            }
        });
        
        res.send([sales,reservationsCount,guestCount])
    } catch (e) {
        res.status(401).send('An error occurred.')
    }
});

ReservationRoute.patch('/cancelreservation', async(req,res)=>{
    const update = {
        status: "Cancelled by Admin"
    }
    try{
        const reservation = await ReservationModel.findByIdAndUpdate(req.body.id,update,{new:true});
        res.send(reservation);
    }catch (e){
        res.status(401).send('An error occurred. Please try again.');
    }
});

module.exports = ReservationRoute;